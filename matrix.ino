// Various registers of the MAX2719. Datasheet https: //datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf.
const byte MAX7219_DECODEMODE = 0x09;
const byte MAX7219_INTENSITY = 0x0a;
const byte MAX7219_SCANLIMIT = 0x0b;
const byte MAX7219_SHUTDOWN = 0x0c;
const byte MAX7219_DISPTEST = 0x0f;

// Store update delay and number of iterations button has been held for.
const int DELAY = 250;
const int HOLD_LIMIT = 15;
int HOLD_CURRENT = 0;

// Screen pins for LED matrix.
const int DIN_PIN = 10;
const int CS_PIN = 11;
const int CLK_PIN = 12;
// Joystick pins VRx, VRy and SW respectively.
const int X_PIN = 0;
const int Y_PIN = 1;
const int SW_PIN = 7;

// Store default joystick position, to allow for -500 to 500 range instead of 0 to 1000.
float X_CENTER;
float Y_CENTER;
// Update later with either -1, 0 or 1.
int X_DIRECTION;
int Y_DIRECTION;
// Store current cursor location for DRAWING onscreen.
int CURSOR_X = 3;
int CURSOR_Y = 3;

// Enable built-in reset function.
void(* resetMemory) (void) = 0;
// Store R symbol for resetting.
const unsigned char LETTER_R[] = {
  B11111110,
  B11000111,
  B11000111,
  B11001110,
  B11111100,
  B11001110,
  B11000111,
  B11000011
};

// Editable memory for LED matrix.
unsigned char DRAWING[] = {
  B00000000,
  B00000000,
  B00000000,
  B00010000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
};

void displayImage(char input[]) {
  // Set a "character" for each "digit" of the displayu.
  setRegister(0x01, input[0]);
  setRegister(0x02, input[1]);
  setRegister(0x03, input[2]);
  setRegister(0x04, input[3]);
  setRegister(0x05, input[4]);
  setRegister(0x06, input[5]);
  setRegister(0x07, input[6]);
  setRegister(0x08, input[7]);
}

// Read an LED at a location, and write it to the opposite.
void invertPixel(int PIXEL_X, int PIXEL_Y) {
  bitWrite(DRAWING[PIXEL_Y], 7 - CURSOR_X, !bitRead(DRAWING[PIXEL_Y], 7 - PIXEL_X));
};

void setup() {
  // Output to display pins and onboard LED.
  pinMode(DIN_PIN, OUTPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(CS_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  // Enabe button on joystick.
  pinMode(7, INPUT_PULLUP);

  // Display all eight "digits", in this case 8 lines on a matrix.
  setRegister(MAX7219_SCANLIMIT, 0x07);
  // Do not attempt to decode digits for seven segment display.
  setRegister(MAX7219_DECODEMODE, 0x00);
  // Device is shut down (0x00) by default. Switch to normal operation (0x11).
  setRegister(MAX7219_SHUTDOWN, 0X01);
  // Set brightness level. [Lowest = 0, Highest = 15]
  setRegister(MAX7219_INTENSITY, 0x00);
  // Enable to fully light display.
  setRegister(MAX7219_DISPTEST, 0x00);

  // Set center numbers to get values around -500 to 500 instead of 0 - 1000.
  X_CENTER = analogRead(X_PIN);
  Y_CENTER = analogRead(Y_PIN);
}

void loop() {
  // Toggle onboard LED every iteration.
  digitalWrite(LED_BUILTIN, HIGH);

  // Receive input from joystick.
  // Reduce range 0 to ~1024 into -500 to 500, and then -1 to 1.
  X_DIRECTION = round((analogRead(X_PIN) - X_CENTER) / X_CENTER);
  Y_DIRECTION = round((analogRead(Y_PIN) - Y_CENTER) / Y_CENTER);

  // Locate old cursor and invert to "delete" it.
  invertPixel(CURSOR_X, CURSOR_Y);
  // Update cursor with movement values, ensuring it doesn't leave the grid.
  CURSOR_X = constrain(CURSOR_X + X_DIRECTION, 0, 7);
  CURSOR_Y = constrain(CURSOR_Y + Y_DIRECTION, 0, 7);
  // Locate the new cursor and invert it to "create" it.
  invertPixel(CURSOR_X, CURSOR_Y);

  // When button is pushed, "draw" on pixel and count how many iterations it has been held for.
  if (!digitalRead(SW_PIN)) {
    invertPixel(CURSOR_X, CURSOR_Y);
    ++HOLD_CURRENT;
  }
  // Reset all iterations once button is lifted.
  else
    HOLD_CURRENT = 0;

  // If button has been held for extended period, display an R temporarily and reset memory.
  if (HOLD_CURRENT == HOLD_LIMIT) {
    displayImage(LETTER_R);
    delay(DELAY * 5);
    resetMemory();
  }

  // Update screen image, and pause for screen delay.
  displayImage(DRAWING);

  // Ensure that the LED is turned off for the same amount of time it is turned on.
  delay(DELAY / 2);
  digitalWrite(LED_BUILTIN, LOW);
  delay(DELAY / 2);
}

// Functions to handle writing to the display.
void setRegister(byte reg, byte value)
{
  digitalWrite(CS_PIN, LOW);

  putByte(reg);
  putByte(value);

  digitalWrite(CS_PIN, LOW);
  digitalWrite(CS_PIN, HIGH);
}
void putByte(byte data)
{
  byte i = 8;
  byte mask;
  while (i > 0)
  {
    mask = 0x01 << (i - 1);
    digitalWrite(CLK_PIN, LOW);

    if (data & mask)
      digitalWrite(DIN_PIN, HIGH);
    else
      digitalWrite(DIN_PIN, LOW);

    digitalWrite(CLK_PIN, HIGH);
    --i;
  }
}

